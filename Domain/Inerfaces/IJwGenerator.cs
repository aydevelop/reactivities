﻿using Domain;

namespace Application.Inerfaces
{
    public interface IJwGenerator
    {
        string CreateToken(AppUser user);
    }
}
