﻿namespace Domain.Inerfaces
{
    public interface IUserAccessor
    {
        string GetCurrentUsername();
    }
}
