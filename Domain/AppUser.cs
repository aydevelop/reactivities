﻿
using System.Collections.Generic;

namespace Domain
{
    public class AppUser : Microsoft.AspNetCore.Identity.IdentityUser
    {
        public string DisplayName { get; set; }
        public string Bio { get; set; }
        public virtual ICollection<UserActivity> UserActivities { get; set; }
        public virtual ICollection<Domain.Photo.Photo> Photos { get; set; }

    }
}
