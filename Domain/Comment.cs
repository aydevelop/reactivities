﻿using System;

namespace Domain
{
    public class Comment
    {
        public int Id { get; set; }
        public string Body { get; set; }
        virtual public AppUser Author { get; set; }
        virtual public Activity Activity { get; set; }
        virtual public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }
}
