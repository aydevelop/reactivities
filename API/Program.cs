using Domain;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Persistence;
using System;
using System.Threading.Tasks;

namespace API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();

            var host = CreateHostBuilder(args).Build();
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            var context = services.GetRequiredService<DataContext>();
            var logger = services.GetRequiredService<ILogger<Program>>();
            var userManager = services.GetRequiredService<UserManager<AppUser>>();

            //var serviceProvider = new ServiceCollection().BuildServiceProvider();
            //var logger2 = serviceProvider.GetService<ILoggerFactory>()
            //            .CreateLogger<Program>();

            try
            {
                await context.Database.MigrateAsync();
                await Seed.SeedData(context, userManager);
                logger.LogInformation("Migration completed");
            }
            catch (Exception ex)
            {

                logger.LogError(ex, "An error occured during migration");
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
