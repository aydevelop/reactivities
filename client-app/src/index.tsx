import React from 'react';
import ReactDOM from 'react-dom';
import 'app/layout/styles.css';
import 'react-toastify/dist/ReactToastify.min.css';
import 'react-widgets/dist/css/react-widgets.css';
import App from 'app/layout/App';
import { Route, BrowserRouter } from 'react-router-dom';
import ScrollToTop from 'app/layout/ScrollToTop';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

// ReactDOM.render(
//   <BrowserRouter>
//     <ScrollToTop>
//       <Route history={history}>
//         <App />
//       </Route>
//     </ScrollToTop>
//   </BrowserRouter>,
//   document.getElementById('root')
// );

ReactDOM.render(
  <BrowserRouter>
    <ScrollToTop>
      <App />
    </ScrollToTop>
  </BrowserRouter>,
  document.getElementById('root')
);
