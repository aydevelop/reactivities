import React, { useState } from 'react';
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Label,
} from 'semantic-ui-react';
import { Link, useHistory } from 'react-router-dom';
import { RootStoreContext } from '../../stores/rootStore';
import { IUserFormValues } from 'app/models/user';

const Login = () => {
  let history = useHistory();
  const rootStore = React.useContext(RootStoreContext);
  const { login } = rootStore.userStore;

  const [email, setEmail] = React.useState('');
  const [pass, setPass] = React.useState('');
  const [error, setError] = React.useState(false);

  const onSubmit = () => {
    let data: IUserFormValues = { email: email, password: pass };
    login(data, history)
      .catch((error) => {
        setError(true);
      })
      .then(() => {
        //history.push('/activities');
      });
  };

  return (
    <Grid textAlign='center' verticalAlign='middle' className='app'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' icon color='black' textAlign='center'>
          Login
        </Header>
        <Form onSubmit={onSubmit} size='large'>
          <Segment stacked>
            <Form.Input
              fluid
              name='email'
              icon='mail'
              iconPosition='left'
              placeholder='Email Address'
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              type='email'
              value={email}
            />

            <Form.Input
              fluid
              name='password'
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              onChange={(e) => {
                setPass(e.target.value);
              }}
              type='password'
              value={pass}
            />

            {error && (
              <div>
                <Label color='red' basic content='Unauthorized' />
              </div>
            )}

            <br />
            <Button disabled={false} color='green' fluid size='small'>
              Submit
            </Button>
          </Segment>
        </Form>

        <Message>
          Don't have an account <Link to='/register'>Register</Link>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default Login;
