import React, { useContext } from 'react';
import { Menu, Container, Button, Dropdown, Image } from 'semantic-ui-react';
import ActivityStore from 'app/stores/activityStore';
import { observer } from 'mobx-react-lite';
import { Link, useLocation } from 'react-router-dom';
import { RootStoreContext } from 'app/stores/rootStore';

interface IProps {
  openCreatetForm: () => void;
}

const NavBar: React.FC<IProps> = ({ openCreatetForm }) => {
  const activityStore = useContext(ActivityStore);
  const rootStore = useContext(RootStoreContext);
  const { user, isLoggedIn, logout } = rootStore.userStore;
  let location = useLocation();

  // React.useEffect(() => {
  //   // setInterval(() => {
  //   //   console.log('rootStore.userStore ' + rootStore.userStore.isLoggedIn);
  //   // }, 2000);
  // }, [rootStore.userStore.isLoggedIn]);

  return (
    <Menu inverted>
      <Container>
        <Menu.Item header as={Link} to='/'>
          <img src='/assets/logo.png' alt='logo' style={{ marginRight: 40 }} />
          Reactivities
        </Menu.Item>
        <Menu.Item name='Activities' as={Link} to='/activities' />
        <Menu.Item>
          <Button
            onClick={activityStore.openCreateForm}
            positive
            content='Create Activity'
            size='mini'
          />
        </Menu.Item>
        {user && (
          <Menu.Item position='right'>
            <Image
              avatar
              spaced='right'
              src={user?.image || '/assets/user.png'}
            />
            <Dropdown pointing='top left' text={user.displayName}>
              <Dropdown.Menu>
                <Dropdown.Item
                  as={Link}
                  to={`/profile/${user.username}`}
                  text='My profile'
                  icon='user'
                />
                <Dropdown.Item onClick={logout} text='Logout' icon='power' />
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Item>
        )}
      </Container>
    </Menu>
  );
};

export default observer(NavBar);
