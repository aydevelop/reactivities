import { IActivity } from 'app/models/activity';
import React, { useContext, useState } from 'react';
import { Segment, Form, Button, ButtonOr } from 'semantic-ui-react';
import { v4 as uuid } from 'uuid';
import ActivityStore from 'app/stores/activityStore';
import { observer } from 'mobx-react-lite';
import { Form as FinalForm, Field } from 'react-final-form';
import TextInput from 'app/common/form/TextInput';
import TextAreaInput from 'app/common/form/TextAreaInput';
import SelectInput from 'app/common/form/SelectInput';
import { category } from 'app/common/form/CategoryOptions';
import DateInput from 'app/common/form/DateInput';

interface IProps {
  setEditMode: (editMode: boolean) => void;
  activity: IActivity;
  createActivity: (activity: IActivity) => void;
  editActivity: (activity: IActivity) => void;
  submitting: boolean;
}

const ActivityForm: React.FC<IProps> = (props) => {
  // const initializeForm = () => {
  //   if (props.activity) {
  //     return props.activity;
  //   }

  //   return {
  //     id: '',
  //     title: '',
  //     category: '',
  //     description: '',
  //     date: '',
  //     city: '',
  //     venue: '',
  //   };
  // };

  const activityStore = useContext(ActivityStore);
  const [activity, setActivity] = useState<any>({
    ...activityStore.selectedActivity,
  });

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setActivity({ ...activity, [name]: value });
  };

  const handleSubmit = () => {
    if (!activity?.id) {
      let newActivity = {
        ...activity,
        id: uuid(),
      };
      //props.createActivity(newActivity);
      activityStore.createActivity(newActivity);
    } else {
      //props.editActivity(activity);
      activityStore.editActivity(activity);
    }
  };

  const handleFinalFormSubmit = (values: any) => {
    console.log('values: ' + JSON.stringify(values));
  };

  return (
    <Segment clearing>
      <Form onSubmit={handleSubmit}>
        <Form.Input
          onChange={handleInputChange}
          name='title'
          placeholder='Title'
          value={activity.title}
        />
        <Form.TextArea
          onChange={handleInputChange}
          name='description'
          rows={2}
          placeholder='Description'
          value={activity.description}
        />
        <Form.Input
          onChange={handleInputChange}
          name='category'
          placeholder='Category'
          value={activity.category}
        />
        <Form.Input
          onChange={handleInputChange}
          name='date'
          type='datetime-local'
          placeholder='Date'
          value={activity.date}
        />
        <Form.Input
          onChange={handleInputChange}
          name='city'
          placeholder='City'
          value={activity.city}
        />
        <Form.Input
          onChange={handleInputChange}
          name='venue'
          placeholder='Venue'
          value={activity.venue}
        />
        <Button
          onClick={() => {
            handleSubmit();
          }}
          floated='right'
          positive
          type='submit'
          content='Submit'
        />
        <Button
          onClick={() => {
            activityStore.cancelSelectedActivity();
          }}
          floated='right'
          type='button'
          content='Cancel'
        />
      </Form>
    </Segment>
  );

  // return (
  //   <Segment>
  //     <FinalForm
  //       onSubmit={handleFinalFormSubmit}
  //       render={({ handleSubmit }) => (
  //         <Form onSubmit={handleSubmit}>
  //           <Field
  //             name='title'
  //             placeholder='Title'
  //             value={activity.title}
  //             component={TextInput}
  //           />
  //           <Field
  //             name='description'
  //             placeholder='Description'
  //             rows={3}
  //             value={activity.description}
  //             component={TextAreaInput}
  //           />
  //           <Field
  //             name='category'
  //             placeholder='Category'
  //             value={activity.description}
  //             component={SelectInput}
  //             options={category}
  //           />
  //           {/* <Field
  //             component={TextInput}
  //             name='date'
  //             placeholder='Date'
  //             value={activity.date}
  //           /> */}
  //           <Field
  //             name='city'
  //             placeholder='City'
  //             value={activity.date}
  //             component={TextInput}
  //           />
  //           <Field
  //             name='venue'
  //             placeholder='Venue'
  //             value={activity.date}
  //             component={TextInput}
  //           />
  //           <Button.Group widths={2}>
  //             <Button
  //               onClick={() => {
  //                 handleSubmit();
  //                 //props.setEditMode(false);
  //               }}
  //               color='blue'
  //               basic
  //               type='submit'
  //               content='Submit'
  //             />
  //             <Button
  //               basic
  //               color='grey'
  //               onClick={() => {
  //                 activityStore.cancelSelectedActivity();
  //               }}
  //               type='button'
  //               content='Cancel'
  //             />
  //           </Button.Group>
  //         </Form>
  //       )}
  //     />
  //   </Segment>
  // );
};

export default observer(ActivityForm);
