import { IActivity } from 'app/models/activity';
import React, { useContext } from 'react';
import { Grid } from 'semantic-ui-react';
import ActivityDetails from '../details/ActivityDetails';
import ActivityForm from '../form/ActivityForm';
import ActivityList from './ActivityList';
import ActivityStore from 'app/stores/activityStore';
import { observer } from 'mobx-react-lite';

interface IProps {
  activities: IActivity[];
  selectActivity: (id: string) => void;
  selectedActivity: IActivity | null;
  editMode: boolean;
  setEditMode: (editMode: boolean) => void;
  setSelectedActivity: (activity: IActivity | null) => void;
  createActivity: (activity: IActivity) => void;
  editActivity: (activity: IActivity) => void;
  deleteActivity: (id: string) => void;
  submitting: boolean;
}

const ActivityDashboard: React.FC<IProps> = (props) => {
  const activityStore = useContext(ActivityStore);
  return (
    <Grid>
      <Grid.Column width={10}>
        <ActivityList
          activities={activityStore.activitiesByDate}
          selectActivity={props.selectActivity}
          deleteActivity={props.deleteActivity}
          submitting={props.submitting}
        />
        {/* <List>
          {props.activities.map((value: IActivity) => (
            <List.Item key={value.id}>{value.title}</List.Item>
          ))}
        </List> */}
      </Grid.Column>
      <Grid.Column width={6}>
        {/* <ActivityDetails selectedActivity={props.selectedActivity!} /> */}
        {!activityStore.editMode && activityStore.selectedActivity && (
          <ActivityDetails
            selectedActivity={props.selectedActivity!}
            setEditMode={props.setEditMode}
            setSelectedActivity={props.setSelectedActivity}
          />
        )}
        {activityStore.editMode && (
          <ActivityForm
            submitting={props.submitting}
            key={props.selectedActivity?.id || 0}
            setEditMode={props.setEditMode}
            activity={props.selectedActivity!}
            createActivity={props.createActivity}
            editActivity={props.editActivity}
          />
        )}
      </Grid.Column>
    </Grid>
  );
};

export default observer(ActivityDashboard);
