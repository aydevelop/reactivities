import React, { useContext } from 'react';
import { Item, Button, Label, Segment } from 'semantic-ui-react';
import { IActivity } from 'app/models/activity';
import ActivityStore from 'app/stores/activityStore';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import ActivityAttendees from 'app/features/home/ActivityAttendees';

interface IProps {
  activities: IActivity[];
  selectActivity: (id: string) => void;
  deleteActivity: (id: string) => void;
  submitting: boolean;
}

const ActivityList: React.FC<IProps> = ({
  activities,
  selectActivity,
  deleteActivity,
  submitting,
}) => {
  const activityStore = useContext(ActivityStore);
  return (
    <Segment clearing>
      <Item.Group divided>
        {activityStore.activitiesByDate.map((activity) => (
          <Item key={activity.id}>
            <Item.Content>
              <Item.Header as={Link} to={`/activities/${activity.id}`}>
                {activity.title}
              </Item.Header>
              <Item.Meta>{format(activity.date, 'eeee - do - MMMM')}</Item.Meta>
              <Item.Description>
                <div>{activity.description}</div>
                <div>
                  {activity.city}, {activity.venue} #######
                </div>
              </Item.Description>
              <Segment secondary>
                <ActivityAttendees attendees={activity.attendees} />
              </Segment>

              <Item.Extra>
                <Button
                  loading={submitting}
                  onClick={(e) => {
                    activityStore.deleteActivity(e, activity.id);
                  }}
                  floated='right'
                  content='Delete'
                  color='red'
                />
                <Button
                  loading={submitting}
                  onClick={() => {
                    //selectActivity(activity.id);
                    activityStore.selectActivity(activity.id);
                  }}
                  floated='right'
                  content='View'
                  color='blue'
                />
                <Label basic content={activity.category} />
              </Item.Extra>
            </Item.Content>
          </Item>
        ))}
      </Item.Group>
    </Segment>
  );
};

export default observer(ActivityList);
