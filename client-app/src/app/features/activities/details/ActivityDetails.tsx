import { IActivity } from 'app/models/activity';
import React, { useContext } from 'react';
import { Card, Image, Button } from 'semantic-ui-react';
import ActivityStore from 'app/stores/activityStore';
import { observer } from 'mobx-react-lite';

interface IProps {
  selectedActivity: IActivity;
  setEditMode: (editMode: boolean) => void;
  setSelectedActivity: (activity: IActivity | null) => void;
}

const ActivityDetails: React.FC<IProps> = ({
  selectedActivity,
  setEditMode,
  setSelectedActivity,
}) => {
  // if (!selectedActivity) {
  //   return <></>;
  // }
  const activityStore = useContext(ActivityStore);
  return (
    <Card fluid>
      <Image
        src={`/assets/categoryImages/${activityStore.selectedActivity?.category}.jpg`}
        wrapped
        ui={false}
      />
      <Card.Content>
        <Card.Header>{activityStore.selectedActivity?.title}</Card.Header>
        <Card.Meta>
          <span>{selectedActivity?.date}</span>
        </Card.Meta>
        <Card.Description>{selectedActivity?.description}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button.Group widths={2}>
          <Button
            onClick={() => {
              activityStore.openEditForm(activityStore.selectedActivity!.id);
              //setEditMode(true);
            }}
            basic
            color='blue'
            content='Edit'
          />
          <Button
            onClick={() => {
              activityStore.cancelSelectedActivity();
              //setSelectedActivity(null);
            }}
            basic
            color='grey'
            content='Cancel'
          />
        </Button.Group>
      </Card.Content>
    </Card>
  );
};

export default observer(ActivityDetails);
