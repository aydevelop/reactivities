import { IAttendee } from 'app/models/activity';
import React from 'react';
import { List, Image } from 'semantic-ui-react';

interface IProps {
  attendees: any[];
}

const ActivityAttendees: React.FC<IProps> = ({ attendees }) => {
  return (
    <div style={{ display: 'flex' }}>
      {attendees.map((item, index) => {
        return (
          <div key={index} style={{ margin: '0px 10px', textAlign: 'center' }}>
            <Image
              size='mini'
              circular
              src={item.image || '/assets/user.png'}
            />
            <span>{item.username}</span>
          </div>
        );
      })}
    </div>

    // <List horizontal>
    //   {'::: ' + JSON.stringify(attendees)}
    //   {/* {attendees.map((item, index) => (
    //     <div key={index + item.displayName}>1 {JSON.stringify(attendees)}</div>
    //   ))} */}
    //   {/* <List.Item>
    //     <Image size='mini' circular src={'/assets/user.png'} />
    //   </List.Item>
    //   <List.Item>
    //     <Image size='mini' circular src={'/assets/user.png'} />
    //   </List.Item>
    //   <List.Item>
    //     <Image size='mini' circular src={'/assets/user.png'} />
    //   </List.Item> */}
    // </List>
  );
};

export default ActivityAttendees;
