import React, { useContext } from 'react';
import { Card, Image, Button, Segment, List } from 'semantic-ui-react';
import ActivityStore from '../../../app/stores/activityStore';
import { observer } from 'mobx-react-lite';
import { Link, RouteComponentProps } from 'react-router-dom';
import ChatComponent from 'app/features/home/Chat';
import ActivityAttendees from './ActivityAttendees';

interface IProps {
  id: string;
}

const ActivityDetails: React.FC<RouteComponentProps<IProps>> = ({
  match,
  history,
  location,
}) => {
  const activityStore = useContext(ActivityStore);
  const {
    detailActivity,
    openEditForm,
    cancelSelectedActivity,
  } = activityStore;

  const loadAct = async () => {
    await activityStore.loadActivity(match.params.id);
  };

  React.useEffect(() => {
    loadAct();
  }, []);

  if (!detailActivity) {
    return <>Loading....</>;
  }

  return (
    <div style={{ paddingBottom: '60px' }}>
      <Card fluid style={{ marginBottom: '40px' }}>
        <Image
          src={`/assets/categoryImages/${detailActivity!.category}.jpg`}
          wrapped
          ui={false}
        />
        <Card.Content>
          <Card.Header>{detailActivity!.title}</Card.Header>
          <Card.Meta>
            <span>{detailActivity!.date}</span>
          </Card.Meta>
          <br />
          <Card.Description>{detailActivity!.description}</Card.Description>
          <br />
          <Card.Description>
            <b>Category:</b> {detailActivity!.category}
          </Card.Description>
          <br />
          <Card.Description>
            <b>Venue:</b> {detailActivity!.venue}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <div
            className='ui tree buttons'
            style={{ display: 'flex', justifyContent: 'right' }}
          >
            <Button className='dropdownWhite' color='green'>
              Join Acivity
            </Button>
            <Button className='dropdownWhite' secondary>
              Cancel attendance
            </Button>
            {/* <Button
              as={Link}
              to={`/manage/${detailActivity.id}`}
              className='dropdownWhite'
              secondary
            >
              Manage Event
            </Button> */}
          </div>
        </Card.Content>
      </Card>
      <Segment
        textAlign='center'
        style={{
          border: 'none',
          marginTop: '20px',
          color: 'black',
          fontWeight: 'bold',
        }}
        attached='top'
        secondary
      >
        {detailActivity?.attendees?.length + ' People Going' ||
          '0 People Going'}
      </Segment>
      {/* <Segment>
        <ActivityAttendees />
      </Segment> */}

      <Segment style={{ marginBottom: '40px' }} inverted>
        <List divided inverted relaxed>
          {detailActivity?.attendees.map((item) => {
            return (
              <List.Item>
                <List.Content>
                  <List.Header>{item.displayName}</List.Header>
                  {item.username}
                </List.Content>
              </List.Item>
            );
          })}

          {/* <List.Item>
            <List.Content>
              <List.Header>Snickerdoodle</List.Header>
              An excellent companion
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>Poodle</List.Header>A poodle, its pretty basic
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>Paulo</List.Header>
              He's also a dog
            </List.Content>
          </List.Item> */}
        </List>
      </Segment>

      <ChatComponent />
    </div>
  );
};

export default observer(ActivityDetails);
