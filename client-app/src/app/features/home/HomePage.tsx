import React, { Fragment, useContext } from 'react';
import { Container, Segment, Header, Button, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { RootStoreContext } from 'app/stores/rootStore';

const HomePage = () => {
  const rootStore = useContext(RootStoreContext);
  const { user, isLoggedIn } = rootStore.userStore;

  return (
    <Segment inverted textAlign='center' vertical className='masthead'>
      <Container text className='mastheadContent'>
        <Header as='h1' inverted>
          Reactivities
        </Header>
        {/* <Header as='h2' inverted content='Welcome to Reactivities' /> */}
        <Button as={Link} to='/activities' size='huge' inverted>
          Welcome
        </Button>
        {!isLoggedIn && !user && (
          <div>
            <br />
            <Fragment>
              <Button as={Link} to='/login' size='huge' inverted>
                Login
              </Button>
            </Fragment>
          </div>
        )}
      </Container>
    </Segment>
  );
};

export default HomePage;
