import agent from 'app/api/agent';
import { createAttendee } from 'app/common/helper';
import { IActivity } from 'app/models/activity';
import {
  action,
  computed,
  configure,
  makeAutoObservable,
  observable,
  runInAction,
} from 'mobx';
import { createContext, SyntheticEvent } from 'react';

//configure({ enforceActions: 'always' });

class ActivityStore {
  constructor() {
    makeAutoObservable(this);
  }

  @observable activityRegistry = new Map();
  @observable title = '';
  @observable activities: IActivity[] = [];
  @observable loadingInitial = false;
  @observable selectedActivity: IActivity | undefined;
  @observable editMode = false;
  @observable submitting = false;
  @observable target = '';
  @observable detailActivity: IActivity | undefined;

  @action loadActivity = async (id: string) => {
    this.loadingInitial = false;
    let activity = this.activityRegistry.get(id);

    if (activity) {
      this.detailActivity = activity;
    } else {
      try {
        activity = await agent.Activities.details(id);
        this.detailActivity = activity;
        this.loadingInitial = false;
      } catch (error) {
        this.loadingInitial = false;
        console.log(error);
      }
    }
  };

  @action deleteActivity = async (
    event: SyntheticEvent<HTMLButtonElement>,
    id: string
  ) => {
    this.submitting = true;
    this.target = event.currentTarget.name;

    try {
      await agent.Activities.delete(id);
      this.activityRegistry.delete(id);
    } catch (error) {
      console.log(error);
    }

    this.target = '';
    this.submitting = false;
  };

  @computed get activitiesByDate() {
    //let arr = [...this.activities];
    let arr = Array.from(this.activityRegistry.values());
    arr.reverse();
    //arr.sort((a, b) => Date.parse(a.date) - Date.parse(b.date));
    return arr;
  }

  @action openEditForm = (id: string) => {
    this.selectedActivity = this.activityRegistry.get(id);
    this.editMode = true;
  };

  @action cancelSelectedActivity = () => {
    this.selectedActivity = undefined;
    this.editMode = false;
  };

  @action cancelFormOpen = () => {
    this.editMode = false;
  };

  @action editActivity = async (activity: IActivity) => {
    this.submitting = true;

    try {
      await agent.Activities.update(activity);
      this.activityRegistry.set(activity.id, activity);
      this.selectedActivity = activity;
      this.editMode = false;
    } catch (err) {
      console.log(err);
    }

    this.submitting = false;
  };

  @action loadActivities = async () => {
    this.loadingInitial = true;

    agent.Activities.list()
      .then((data) => {
        data.forEach((act: any) => {
          act.date = act.date.split('.')[0];
          this.activities.push(act);
          this.activityRegistry.set(act.id, act);
        });
      })
      .catch((error) => console.log(error))
      .finally(() => (this.loadingInitial = false));
  };

  @action selectActivity = (id: string) => {
    //let res = this.activities.find((a) => a.id === id);
    //this.selectedActivity = res;

    this.selectedActivity = this.activityRegistry.get(id);
    this.editMode = false;
    console.log('id: ' + id);
    console.log('selectActivity: ' + JSON.stringify(this.selectedActivity));
    console.log(JSON.stringify(this.activities));
  };

  @action createActivity = async (activity: IActivity) => {
    this.submitting = true;
    this.loadingInitial = true;

    try {
      await agent.Activities.create(activity);
      this.activities.push(activity);
      this.activityRegistry.set(activity.id, activity);
    } catch (error) {
      console.log(error);
    }

    this.editMode = false;
    this.submitting = false;
    this.loadingInitial = false;
  };

  @action openCreateForm = () => {
    this.editMode = true;
    this.selectedActivity = undefined;
  };

  // @action attendActivity = async () => {
  //   const attendee = createAttendee(this.rootStore.userStore.user!);
  //   this.loading = true;
  //   try {
  //     await agent.Activities.attend(this.activity!.id);
  //     runInAction(() => {
  //       if (this.activity) {
  //         this.activity.attendees.push(attendee);
  //         this.activity.isGoing = true;
  //         this.activityRegistry.set(this.activity.id, this.activity);
  //         this.loading = false;
  //       }
  //     })
  //   } catch (error) {
  //     runInAction(() => {
  //       this.loading = false;
  //     })
  //     toast.error('Problem signing up to activity');
  //   }
  // };
}

export default createContext(new ActivityStore());
