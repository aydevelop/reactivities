import {
  observable,
  computed,
  action,
  runInAction,
  makeAutoObservable,
} from 'mobx';
import { IUser, IUserFormValues } from '../models/user';
import agent from '../api/agent';
import { RootStore } from './rootStore';

export default class UserStore {
  rootStore: RootStore;
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }

  @observable user: IUser | null = null;

  @computed get isLoggedIn() {
    return !!this.user;
  }

  @action login = async (values: IUserFormValues, history: any) => {
    try {
      const user = await agent.User.login(values);
      runInAction(() => {
        this.user = user;
        this.rootStore.commonStore.setToken(user.token);

        history.push('/activities');
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  };

  @action getUser = async () => {
    try {
      const user = await agent.User.current();
      runInAction(() => {
        this.user = user;
      });
    } catch (error) {
      console.log('getUser error: ' + error);
    }
  };

  @action logout = () => {
    this.rootStore.commonStore.setToken(null);
    runInAction(() => {
      this.user = null;
      //console.log('user::;: . ' + this.user);
    });
  };
}
