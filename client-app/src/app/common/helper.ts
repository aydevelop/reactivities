import { IAttendee } from 'app/models/activity';
import { IUser } from 'app/models/user';

export const createAttendee = (user: IUser): IAttendee => {
  return {
    displayName: user.displayName,
    isHost: false,
    username: user.username,
    image: user.image!,
  };
};
