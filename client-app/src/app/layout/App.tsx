import React, { useContext, useEffect, useState } from 'react';
import { Container } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { IActivity } from 'app/models/activity';
import NavBar from 'app/features/nav/NavBar';
import ActivityDashboard from 'app/features/activities/dashboard/ActivityDashboard';
import agent from 'app/api/agent';
import LoadingComponent from './LoadingComponent';
import ActivityStore from '../stores/activityStore';
import { observer } from 'mobx-react-lite';
import { Route, Switch } from 'react-router-dom';
import HomePage from 'app/features/home/HomePage';
import ActtivityForm from 'app/features/activities/form/ActivityForm';
import ActivityForm from 'app/features/activities/form/ActivityForm';
import ActivityDetails from 'app/features/activities/details/ActivityDetails';
import DetailPage from 'app/features/home/DetailPage';
import NotFound from './NotFound';
import { ToastContainer } from 'react-toastify';
import LoginForm from 'app/features/user/LoginForm';
import { RootStoreContext } from 'app/stores/rootStore';

const App = () => {
  const rootStore = useContext(RootStoreContext);
  const { setAppLoaded, token } = rootStore.commonStore;
  const { getUser } = rootStore.userStore;

  const activityStore = useContext(ActivityStore);
  const [submitting, setSubmitting] = useState(false);
  // const [loading, setLoading] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [activities, setActivities] = useState<IActivity[]>([]);
  const [selectedActivity, setSelectedActivity] = useState<IActivity | null>(
    null
  );

  const handleSelectActivity = (id: string) => {
    let a = activities.find((a) => a.id === id);
    if (a) {
      setSelectedActivity(a);
    }
  };

  const handleOpenCreateForm = () => {
    setSelectedActivity(null);
    setEditMode(true);
  };

  const handleCreateActivity = (activity: IActivity) => {
    setSubmitting(true);
    agent.Activities.create(activity)
      .then(() => {
        setActivities([...activities, activity]);
        setSelectedActivity(activity);
        setEditMode(false);
      })
      .then(() => setSubmitting(false));
  };

  const handleDeleteActivity = (id: string) => {
    setSubmitting(true);
    agent.Activities.delete(id)
      .then(() => {
        setActivities([...activities.filter((a) => a.id !== id)]);
        setSelectedActivity(null);
      })
      .then(() => setSubmitting(false));
  };

  const handleEditActivity = (activity: IActivity) => {
    setSubmitting(true);
    agent.Activities.update(activity)
      .then(() => {
        let newArr = activities.map((a) => {
          if (a.id === activity.id) {
            return activity;
          }
          return a;
        });

        setActivities(newArr);
        setSelectedActivity(activity);
        setEditMode(false);
      })
      .then(() => setSubmitting(false));
  };

  useEffect(() => {
    activityStore.loadActivities();
  }, [activityStore]);

  if (activityStore.loadingInitial) {
    return <LoadingComponent content='Loading ...' />;
  }

  return (
    <>
      <ToastContainer position='bottom-right' />
      <Route exact path='/' component={HomePage} render={() => <HomePage />} />
      <Route
        path='/(.+)'
        render={() => (
          <>
            <NavBar openCreatetForm={handleOpenCreateForm} />
            <Container style={{ marginTop: '30px' }}>
              <h1>{activityStore.title}</h1>
              <Switch>
                <Route exact path='/activities' component={ActivityDashboard} />
                <Route exact path='/activities/:id' component={DetailPage} />
                <Route exact path='/createactivity' component={ActivityForm} />
                <Route exact path='/login' component={LoginForm} />
                {/* <Route path='*' render={() => <h2>Not Found</h2>} /> */}
                <Route path='*' component={NotFound} />
              </Switch>
              {/* <ActivityDashboard
          submitting={activityStore.submitting}
          activities={activityStore.activities}
          selectActivity={handleSelectActivity}
          selectedActivity={selectedActivity}
          editMode={editMode}
          setEditMode={setEditMode}
          setSelectedActivity={setSelectedActivity}
          createActivity={handleCreateActivity}
          editActivity={handleEditActivity}
          deleteActivity={handleDeleteActivity}
        /> */}
            </Container>
          </>
        )}
      />
    </>
  );
};

export default observer(App);
