﻿using Application.Errors;
using AutoMapper;
using Domain;
using MediatR;
using Persistence;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Activities
{
    public class Details
    {
        public class Query : IRequest<ActivityDto>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, ActivityDto>
        {
            private readonly DataContext _context;
            private readonly IMapper mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                this.mapper = mapper;
            }

            public async Task<ActivityDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var id = request.Id;

                //var activity = await _context.Activities.
                //     Include(x => x.UserActivities).
                //        ThenInclude(x => x.AppUser).
                //    SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                //Lazy loading

                var activity = await _context.Activities.FindAsync(new object[] { id }, cancellationToken);

                if (activity == null)
                {
                    throw new RestException(HttpStatusCode.NotFound, new
                    {
                        activity = "Not found"
                    });
                }

                var activityToReturn = mapper.Map<Activity, ActivityDto>(activity);
                return activityToReturn;
            }
        }
    }
}
