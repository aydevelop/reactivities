﻿using Application.Errors;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User
{
    public class Register
    {
        public class Command : IRequest
        {
            public string DisplayName { get; set; }
            public string UserName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.DisplayName).NotEmpty();
                RuleFor(x => x.UserName).NotEmpty();
                RuleFor(x => x.Email).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();

                //RuleFor(x => x.Email).NotEmpty().EmailAddress();
                //RuleFor(x => x.Password).Password();
            }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly UserManager<AppUser> userManager;
            //private readonly IJwGenerator jwtGenerator;

            public Handler(DataContext context, UserManager<AppUser> userManager/*, IJwGenerator jwtGenerator*/)
            {
                _context = context;
                this.userManager = userManager;
                //this.jwtGenerator = jwtGenerator;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var any = await _context.Users.AnyAsync(x => x.Email == request.Email, cancellationToken);
                if (any)
                {
                    throw new RestException(HttpStatusCode.BadRequest, new { Email = "Email already exists" });
                }

                var login = await _context.Users.AnyAsync(x => x.UserName == request.UserName, cancellationToken);
                if (login)
                {
                    throw new RestException(HttpStatusCode.BadRequest, new { Username = "Username already exists" });
                }

                var user = new AppUser
                {
                    DisplayName = request.DisplayName,
                    Email = request.Email,
                    UserName = request.UserName
                };

                var result = await userManager.CreateAsync(user, request.Password);
                if (!result.Succeeded) throw new Exception("Problem creating user");

                return Unit.Value;
            }
        }
    }
}
